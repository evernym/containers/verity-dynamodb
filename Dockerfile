FROM amazon/dynamodb-local:latest

COPY docker-entrypoint-tables.d docker-entrypoint-tables.d
COPY entrypoint.sh entrypoint.sh

ENTRYPOINT ["./entrypoint.sh"]