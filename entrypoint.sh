#!/bin/bash

repeat_until_ready()
{
  cmd="$1"
  shift
  args=( "$@" )
  attempt_counter=0
  max_attempts=10
  until $cmd "${args[@]}" 2> /dev/null; do
  if [ ${attempt_counter} -eq ${max_attempts} ];then
    echo "Max attempts reached"
    exit 1
  fi
  echo "Attempting connection... failed."
  attempt_counter=$(($attempt_counter+1))
  sleep .5
done
}

term() {
  echo "Caught SIGTERM signal!"
  kill -TERM "$dynamodb_pid" 2>/dev/null
}

mkdir -p $HOME/data

echo "[Starting dynamoDB-local]"
java -jar DynamoDBLocal.jar -sharedDb -dbPath "$HOME"/data &> "$HOME"/dynamodb.log &
dynamodb_pid=$!
trap term SIGTERM

repeat_until_ready curl -S -s -o /dev/null http://localhost:8000/

if ! [ -f "$HOME"/dynamodb_tables_built ]; then
  if [ -d docker-entrypoint-tables.d/ ]; then
          echo
          echo "[Creating Tables]"
      for table in $(find docker-entrypoint-tables.d/ -type f | sort -n); do
          echo
          echo
          echo "[Creating Table: '$table']"
          echo
          curl 'http://localhost:8000/' \
          -f \
          -H 'X-Amz-Target: DynamoDB_20120810.CreateTable' \
          -H 'Authorization: AWS4-HMAC-SHA256 Credential=cUniqueSessionID/20171230/us-west-2/dynamodb/aws4_request, SignedHeaders=host;x-amz-content-sha256;x-amz-date;x-amz-target;x-amz-user-agent, Signature=42e8556bbc7adc659af8255c66ace0641d9bce213fb788f419c07833169e2af8' \
          --data-binary "@$table"

          rc=$?
          if [ $rc -ne 0 ] ; then
              exit $rc
          fi
      done
      echo
      echo
      echo "[Done creating tables]"
      touch "$HOME"/dynamodb_tables_built
      echo
  fi
fi

echo
echo "[Initialization Complete -- see log at $HOME/dynamodb.log]"
echo
wait "$dynamodb_pid"